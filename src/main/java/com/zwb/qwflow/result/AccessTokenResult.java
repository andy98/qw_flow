package com.zwb.qwflow.result;

import lombok.Data;

/**
 * 获取token结果参数
 */
@Data
public class AccessTokenResult {
    private int errcode;
    private String errmsg;
    private String access_token;
    private int expires_in;
}

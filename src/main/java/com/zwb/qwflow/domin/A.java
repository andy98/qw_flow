package com.zwb.qwflow.domin;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class A {
    private ApplyDataDTO applyData;
    private List<ApproverDTO> approver;
    private String creatorUserid;
    private Integer notifyType;
    private List<String> notifyer;
    private List<SummaryListDTO> summaryList;
    private String templateId;
    private Integer useTemplateApprover;

    @NoArgsConstructor
    @Data
    public static class ApplyDataDTO {
        private List<ContentsDTO> contents;

        @NoArgsConstructor
        @Data
        public static class ContentsDTO {
            private String control;
            private String id;
            private ValueDTO value;

            @NoArgsConstructor
            @Data
            public static class ValueDTO {
                private String text;
            }
        }
    }

    @NoArgsConstructor
    @Data
    public static class ApproverDTO {
        private Integer attr;
        private List<String> userid;
    }

    @NoArgsConstructor
    @Data
    public static class SummaryListDTO {
        private List<SummaryInfoDTO> summaryInfo;

        @NoArgsConstructor
        @Data
        public static class SummaryInfoDTO {
            private String text;
            private String lang;
        }
    }
}

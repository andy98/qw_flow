package com.zwb.qwflow.domin.param;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SearchApproveFiltersDTO {
    private String key;
    private String value;
}

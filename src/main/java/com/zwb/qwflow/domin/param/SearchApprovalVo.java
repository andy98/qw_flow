package com.zwb.qwflow.domin.param;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class SearchApprovalVo {
    private long starttime;
    private long endtime;
    private Integer cursor;
    private Integer size;
    private List<SearchApproveFiltersDTO> filters;

}

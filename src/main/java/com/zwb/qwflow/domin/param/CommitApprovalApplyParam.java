package com.zwb.qwflow.domin.param;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zwb.qwflow.constant.ControlTypeConstant;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @ClassName: CommitApprovalApplyParam
 * @Description:TODO ()
 * @author: lanyanhua
 * @date: 2020/10/14 2:41 下午
 * @Copyright:
 */
@Data
@Accessors(chain = true)
public class CommitApprovalApplyParam {


    /*
     * creator_userid : WangXiaoMing
     * template_id : 3Tka1eD6v6JfzhDMqPd3aMkFdxqtJMc2ZRioeFXkaaa
     * use_template_approver : 0
     * approver : [{"attr":2,"userid":["WuJunJie","WangXiaoMing"]},{"attr":1,"userid":["LiuXiaoGang"]}]
     * notifyer : ["WuJunJie","WangXiaoMing"]
     * notify_type : 1
     * apply_data : {"contents":[{"control":"Text","id":"Text-15111111111","value":{"text":"文本填写的内容"}}]}
     * summary_list : [{"summary_info":[{"text":"摘要第1行","lang":"zh_CN"}]},{"summary_info":[{"text":"摘要第2行","lang":"zh_CN"}]},{"summary_info":[{"text":"摘要第3行","lang":"zh_CN"}]}]
     */

    /**
     * 申请人userid
     */
    private String creator_userid;
    /**
     * 模板id
     */
    private String template_id;
    /**
     * 审批人模式：0-通过接口指定审批人、抄送人（此时approver、notifyer等参数可用）; 1-使用此模板在管理后台设置的审批流程，支持条件审批。默认为0
     */
    private int use_template_approver;
    /**
     * 抄送方式：1-提单时抄送（默认值）； 2-单据通过后抄送；3-提单和单据通过后抄送。仅use_template_approver为0时生效。
     */
    private int notify_type;
    /**
     * 抄送人节点userid列表，仅use_template_approver为0时生效。
     */
    private String[] notifyer;
    /**
     * 审批申请数据，可定义审批申请中各个控件的值，其中必填项必须有值，选填项可为空，数据结构同“获取审批申请详情”接口返回值中同名参数“apply_data”
     */
    private JSONObject apply_data;
    /**
     * 审批流程信息，用于指定审批申请的审批流程，支持单人审批、多人会签、多人或签，可能有多个审批节点，仅use_template_approver为0时生效。
     */
    private JSONArray approver;
    /**
     * 摘要信息，用于显示在审批通知卡片、审批列表的摘要信息，最多3行
     */
    private JSONArray summary_list;

    public CommitApprovalApplyParam() {
        this.use_template_approver = 0;
        this.apply_data = new JSONObject();
        this.approver = new JSONArray();
        this.summary_list = new JSONArray();
    }

    public CommitApprovalApplyParam(String creator_userid, String template_id) {
        this();
        this.creator_userid = creator_userid;
        this.template_id = template_id;
    }

    public void  setNotifyer(String... notifyer){
        this.notifyer = notifyer;
    }
    /**
     * 添加审批人
     *
     * @param attr   节点审批方式：1-或签；2-会签，仅在节点为多人审批时有效
     *               会签：指同一个审批节点设置多个人，如ABC三人，三人会同时收到审批，需全部同意之后，审批才可到下一审批节点；
     *               或签：指同一个审批节点设置多个人，如ABC三人，三人会同时收到审批，只要其中任意一人审批即可到下一审批节点；
     * @param userId 审批节点审批人userid列表，若为多人会签、多人或签，需填写每个人的userid
     */
    public void addApprover(int attr, String... userId) {
        JSONObject json = new JSONObject();
        json.put("attr", attr);
        json.put("userid", userId);
        this.approver.add(json);
    }

    /**
     * 审批申请详情
     *
     * @param contents 审批申请详情，由多个表单控件及其内容组成，其中包含需要对控件赋值的信息
     */
    public void setApplyDataContents(JSONObject... contents) {
        // 审批申请详情，由多个表单控件及其内容组成，其中包含需要对控件赋值的信息
        this.apply_data.put("contents", contents);
    }
    /**
     * 摘要信息，用于显示在审批通知卡片、审批列表的摘要信息，最多3行
     *
     * @param text 摘要行显示文字，用于记录列表和消息通知的显示，不要超过20个字符
     */
    public void setSummaryInfo(String... text) {
        for (String s : text) {
            JSONArray summaryInfoArr = new JSONArray();
            JSONObject textObj = new JSONObject();
            textObj.put("text",s);
            textObj.put("lang","zh_CN");
            summaryInfoArr.add(textObj);
            JSONObject summaryInfo = new JSONObject();
            summaryInfo.put("summary_info",summaryInfoArr);
            // 摘要信息，用于显示在审批通知卡片、审批列表的摘要信息，最多3行
            this.summary_list.add(summaryInfo);
        }
    }

    /**
     * 文本/多行文本控件（control参数为Text或Textarea）
     *
     * @param id   控件ID
     * @param text 文本内容，在此填写文本/多行文本控件的输入值
     */
    public JSONObject controlText(String id, String text) {
        JSONObject value = new JSONObject();
        value.put("text", text);

        return createControl(id, ControlTypeConstant.text, value);
    }

    /**
     * 数字控件（control参数为Number）
     *
     * @param id         控件ID
     * @param new_number 数字内容，在此填写数字控件的输入值
     */
    public JSONObject controlNumber(String id, String new_number) {
        JSONObject value = new JSONObject();
        value.put("new_number", new_number);

        return createControl(id, ControlTypeConstant.number, value);
    }

    /**
     * 金额控件（control参数为Money）
     *
     * @param id        控件ID
     * @param new_money 金额内容，在此填写金额控件的输入值
     */
    public JSONObject controlMoney(String id, String new_money) {
        JSONObject value = new JSONObject();
        value.put("new_money", new_money);

        return createControl(id, ControlTypeConstant.money, value);
    }

    /**
     * 日期/日期+时间控件（control参数为Date）
     *
     * @param id          控件ID
     * @param type        时间展示类型：day-日期；hour-日期+时间 ，和对应模板控件属性一致
     * @param s_timestamp 时间戳-字符串类型，在此填写日期/日期+时间控件的选择值，以此为准
     */
    public JSONObject controlDate(String id, String type, String s_timestamp) {
        //{
        //    "date": {
        //        "type": "day",
        //        "s_timestamp": "1569859200"
        //    }
        //}
        JSONObject value = new JSONObject();
        JSONObject date = new JSONObject();
        value.put("date", date);
        date.put("type", type);
        date.put("s_timestamp", s_timestamp);

        return createControl(id, ControlTypeConstant.date, value);
    }

    /**
     * 单选/多选控件（control参数为Selector）
     *
     * @param id   控件ID
     * @param type 选择方式：single-单选；multi-多选
     * @param key  选项key，可通过“获取审批模板详情”接口获得
     */
    public JSONObject controlSelector(String id, String type, String... key) {
        //{
        //    "selector": {
        //        "type": "multi",
        //        "options": [
        //            {
        //                "key": "option-15111111111",
        //            },
        //            {
        //                "key": "option-15222222222",
        //            }
        //        ]
        //    }
        //}
        JSONObject value = new JSONObject();
        JSONObject selector = new JSONObject();
        JSONArray options = new JSONArray();
        value.put("selector", selector);
        selector.put("type", type);
        selector.put("options", options);

        for (String s : key) {
            JSONObject option = new JSONObject();
            option.put("key", s);
            options.add(option);
        }

        return createControl(id, ControlTypeConstant.selector, value);
    }

    /**
     * 成员控件（control参数为Contact，且value参数为members）
     *
     * @param id      控件ID
     * @param userIds 所选成员的userid
     * @param names   成员名
     */
    public JSONObject controlMembers(String id, List<String> userIds, List<String> names) {
        //{
        //    "members": [
        //        {
        //            "userid": "WuJunJie",
        //            "name": "Jackie"
        //        },
        //        {
        //            "userid": "WangXiaoMing"
        //            "name": "Tom"
        //        }
        //    ]
        //}
        JSONObject value = new JSONObject();
        JSONArray members = new JSONArray();
        value.put("members", members);
        for (int i = 0; i < userIds.size(); i++) {
            JSONObject member = new JSONObject();
            member.put("userid", userIds.get(i));
            member.put("name", names.get(i));
            members.add(member);
        }

        return createControl(id, ControlTypeConstant.contact, value);
    }

    /**
     * 部门控件（control参数为Contact，且value参数为departments）
     *
     * @param id        控件ID
     * @param openapiId 所选部门id
     * @param names     所选部门名
     */
    public JSONObject controlDepartments(String id, List<String> openapiId, List<String> names) {
        //{
        //    "departments": [
        //        {
        //            "openapi_id": "2",
        //            "name": "销售部",
        //        },
        //        {
        //            "openapi_id": "3",
        //            "name": "生产部",
        //        }
        //    ]
        //}
        JSONObject value = new JSONObject();
        JSONArray members = new JSONArray();
        value.put("departments", members);
        for (int i = 0; i < openapiId.size(); i++) {
            JSONObject member = new JSONObject();
            member.put("openapi_id", openapiId.get(i));
            member.put("name", names.get(i));
            members.add(member);
        }

        return createControl(id, ControlTypeConstant.contact, value);
    }

    /**
     * 附件控件（control参数为File，且value参数为files）
     *
     * @param id     控件ID
     * @param fileid 文件id，该id为临时素材上传接口返回的的media_id，注：提单后将作为单据内容转换为长期文件存储；目前一个审批申请单，全局仅支持上传6个附件，否则将失败。
     */
    public JSONObject controlFile(String id, String... fileid) {
        //{
        //    "files": [
        //        {
        //            "file_id": "1G6nrLmr5EC3MMb_-zK1dDdzmd0p7cNliYu9V5w7o8K1aaa"
        //        }
        //    ]
        //}
        JSONObject value = new JSONObject();
        JSONArray files = new JSONArray();
        value.put("files", files);
        for (String s : fileid) {

            JSONObject file = new JSONObject();
            file.put("file_id", s);
            files.add(file);
        }

        return createControl(id, ControlTypeConstant.file, value);
    }
    /**
     * 明细控件（control参数为Table）
     *
     * @param id     控件ID
     * @param table children 明细内容，一个明细控件可能包含多个子明细
     *              list 子明细列表，在此填写子明细的所有子控件的值，子控件的数据结构同一般控件
     */
    public JSONObject controlTable(String id, JSONObject[]... table) {
        //{
        //    "children": [
        //        {
        //            "list": [
        //                {
        //                    "control": "Text",
        //                    "id": "Text-15111111111",
        //                    "title": [
        //                        {
        //                            "text": "明细内文本控件",
        //                            "lang": "zh_CN"
        //                        }
        //                    ],
        //                    "value": {
        //                        "text": "明细文本"
        //                    }
        //                }
        //            ]
        //        }
        //    ]
        //}
        JSONObject value = new JSONObject();
        JSONArray rows = new JSONArray();
        value.put("children", rows);
        for (JSONObject[] row : table) {
            JSONObject rowValue = new JSONObject();
            rowValue.put("list",row);
            rows.add(rowValue);
        }
        return createControl(id, ControlTypeConstant.file, value);
    }

    /**
     * table 明细
     * @param cols 这里就是多个控件值调用createControl方法
     * @return 控件数组
     */
    public JSONObject[] tableRow(JSONObject... cols){
        return cols;
    }

    public JSONObject createControl(String id, String type, JSONObject value) {
        JSONObject control = new JSONObject();
        control.put("id", id);
        control.put("control", type);
        control.put("value", value);
        return control;
    }

}

package com.zwb.qwflow.constant;

/**
 * @ClassName: ControlType
 * @Description:TODO ()
 * @author: lanyanhua
 * @date: 2020/10/14 3:03 下午
 * @Copyright:
 */
public class ControlTypeConstant {
    //控件类型：Text-文本；Textarea-多行文本；Number-数字；Money-金额；Date-日期/日期+时间；
    // Selector-单选/多选；；Contact-成员/部门；Tips-说明文字；File-附件；Table-明细；
    public static final String text = "Text";
    public static final String textarea = "Textarea";
    public static final String number = "Number";
    public static final String money = "Money";
    public static final String date = "Date";
    public static final String selector = "Selector";
    public static final String contact = "Tips";
    public static final String file = "File";
    public static final String table = "Table";
}

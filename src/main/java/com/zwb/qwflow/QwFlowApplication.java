package com.zwb.qwflow;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QwFlowApplication {

    public static void main(String[] args) {
        SpringApplication.run(QwFlowApplication.class, args);
    }

}

package com.zwb.qwflow;

import cn.hutool.core.collection.ListUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.zwb.qwflow.constant.CommonConstant;
import com.zwb.qwflow.controller.SendQwController;
import com.zwb.qwflow.domin.param.CommitApprovalApplyParam;
import com.zwb.qwflow.domin.param.SearchApprovalVo;
import com.zwb.qwflow.domin.param.SearchApproveFiltersDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

@SpringBootTest
class QwFlowApplicationTests {


    @Autowired
    private SendQwController sendQwController;


    @Test
    void m1() {
        System.out.println("q");
    }

    @Test
    void getToken() {
        System.out.println(sendQwController.getToken());
    }

    @Test
    void getTemplateDetail() {
        System.out.println(sendQwController.getTemplateDetail(CommonConstant.TEMPLATEID));
    }

    @Test
    void getUserDetail() {
        System.out.println(sendQwController.getUserDetail("ZhangWenBiao"));
        System.out.println(sendQwController.getUserDetail("dzw666"));
        System.out.println(sendQwController.getUserDetail("dzw6667"));
    }

    //提交审批申请
    @Test
    void commitApprovalApply() {
        CommitApprovalApplyParam param = new CommitApprovalApplyParam("dzw666", CommonConstant.TEMPLATEID);
        //添加审批人
        param.addApprover(1, "ZhangWenBiao");
        param.addApprover(2, "ZhangWenBiao");
        //审批申请数据
        param.setApplyDataContents(
                param.controlText("Text-1656405646620", "蛋炒饭"),
                param.controlText("Textarea-1656411173031", "可乐"),
                param.controlSelector("Selector-1656411222519", "multi", "option-1656411222519"),
                param.controlTable("Table-1656411291100", param.tableRow(param.controlText("Text-1656411358094", "要"), param.controlText("Text-1656411375322", "不喝")))
        );
        //摘要信息
        param.setSummaryInfo("哈哈哈哈哈哈哈");
        //抄送人
        param.setNotifyer("ZhangWenBiao");
        param.setNotify_type(2);

        System.out.println(JSON.toJSONString(param));
        System.out.println(sendQwController.commitApprovalApply(param));
    }

    @Test
    void searchApprovalList() throws ParseException {
        DateFormat df=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        SearchApprovalVo searchApprovalVo = new SearchApprovalVo();

        searchApprovalVo.setStarttime(df.parse("2022-06-01 10:00:00").getTime()/1000);
        searchApprovalVo.setEndtime(df.parse("2022-07-02 10:00:00").getTime()/1000);

        searchApprovalVo.setCursor(0);
        searchApprovalVo.setSize(100);

        searchApprovalVo.setFilters(ListUtil.toList(
                new SearchApproveFiltersDTO("template_id",CommonConstant.TEMPLATEID),
                new SearchApproveFiltersDTO("creator","dzw666"),
                new SearchApproveFiltersDTO("department","1"),
                new SearchApproveFiltersDTO("sp_status","2")
                ));

        System.out.println(sendQwController.searchApprovalList(searchApprovalVo));
    }

    @Test
    void getApprovalDetail(){
        System.out.println(sendQwController.getApprovalDetail("202206300003"));
    }

}
